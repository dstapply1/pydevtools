/**
 * 
 */
package PYDevTools.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

import PYDevTools.Spring.SpringUtilities;
import PYDevTools.db.item.Item;
import PYDevTools.db.item.StatSystem;
import PYDevTools.enums.ItemInventoryType;
import PYDevTools.utilities.ImageDrawingComponent;
import PYDevTools.utilities.ItemIconFinder;
import PYDevTools.utilities.ItemToolTip;
import PYDevTools.utilities.MySQLAccess;
import PYDevTools.utilities.SpellFinder;

/**
 * @author alfeey44
 *
 */
@SuppressWarnings("serial")
public class ArmorPanel extends JPanel implements FocusListener, ActionListener, KeyListener {
	
	private static ArmorPanel instance = null;
	// Panels
	private JSplitPane mainPane;
	private JScrollPane leftPane, rightPane;
	private JPanel leftPanel, rightPanel;
	private JLabel itemIconLabel, itemToolTipLabel;
	private JButton createItem, insertItem;
	private ImageIcon anvilImageIcon;
	private ImageDrawingComponent itemIcon;
	private ItemToolTip itemToolTip;
	private String[] labels = { "Entry: ", "Name: ", "Description: ", "Display:", "Quality: ", "Equip: ", "Subclass: ", 
								"Binds: ", "Armor: ", "Block: ", "Duribility: ", "Required Level: ","Item Level: ", "Unique: ",
								"Stat Type 1: ", "Stat Value 1: ",
								"Stat Type 2: ", "Stat Value 2: ", "Stat Type 3: ", "Stat Value 3: ", 
								"Stat Type 4: ", "Stat Value 4: ", "Stat Type 5: ", "Stat Value 5: ", 
								"Stat Type 6: ", "Stat Value 6: ", "Stat Type 7: ", "Stat Value 7: ", "Stat Type 8: ", "Stat Value 8: ", 
								"Stat Type 9: ", "Stat Value 9: ", "Stat Type 10: ", "Stat Value 10: ", "Holy Resist: ",
								"Fire Resist: ", "Nature Resist: ", "Frost Resist: ", "Shadow Resist: ", "Arcane Resist: ",  "Spell 1: ", "Spell 1 Trigger: ", "Spell 2: ", "Spell 2 Trigger: ",
								"Spell 3: ", "Spell 3 Trigger: ", "Spell 4: ", "Spell 4 Trigger: ", "Spell 5: ", "Spell 5 Trigger: ", 
								"Socket 1: ", "Socket 2: ", "Socket 3: ", "Socket Bonus: " };
	private int numLabels = labels.length;
	private JTextField entry, name, desc, display, armor, block, reqlvl, duribility, 
					   statvalue1, statvalue2, statvalue3, statvalue4, statvalue5,
					   statvalue6, statvalue7, statvalue8, statvalue9, statvalue10, 
					   holyres, fireres, natureres, frostres, shadowres, arcaneres,
					   ilvl, unique, spell1, spell2, spell3, spell4, spell5, socketbonus;
	private JComboBox<String> quality, equip, subclass, bind, role, spelltrigger1, spelltrigger2, stattype1, 
							  stattype2, stattype3, stattype4, stattype5, stattype6, 
							  stattype7, stattype8, stattype9, stattype10,
	   						  spelltrigger3, spelltrigger4, spelltrigger5,
	   						  socket1, socket2, socket3;
	private String[] statTypes;
	private String[] qualities = { "Gray", "White", "Green", "Blue", "Purple", "Orange", "Heirloom" };
	private String[] equips = { "Head", "Neck", "Shoulder", "Cloak", "Shirt", "Tabard", "Chest", "Waist", "Legs", "Feet",
								"Wrists", "Hands", "Finger", "Trinket", "Shield",  "Relic"};
	private String[] mainEquips = { "Head", "Shoulder", "Chest", "Wrists", "Hands", "Waist", "Legs", "Feet" };
	private String[] miscEquips = { "Neck", "Ring", "Trinket", "Shirt", "Tabard" };
	private String[] clothEquips = { "Head", "Shoulder", "Cloak", "Chest", "Wrists", "Hands", "Waist", "Legs", "Feet" };
	private String[] shieldEquip = { "Shield" };
	private String[] relicEquip = { "Relic" };
	private String[] subclasses = { "Miscellaneous", "Cloth", "Leather", "Mail", "Plate", "Shield", "Libram", "Idol",
									"Totem", "Sigil" };
	private String[] binds = { "None", "On Pick Up", "On Equip", "On Use" };
	private String[] roles = { "DPS", "Tank", "Healer" };
	private String[] spelltriggers = { "Use", "On Equip", "Chance on Hit" };
	private String[] socketColors = { "None", "Meta", "Red", "Yellow", "Blue", "Prismatic" };
	private JLabel invalidDisplayId;
	
	private Font italicFont = new Font("Arial", Font.ITALIC, 14);
	private SpringLayout rightLayout;
	
	private ItemIconFinder iconFinder = ItemIconFinder.getInstance();
	private SpellFinder spellFinder = SpellFinder.getInstance();
	private Item item = new Item();
	private MySQLAccess db = new MySQLAccess();
	private ServerConfigPanel serverConfigPanel = ServerConfigPanel.getInstance();
	private StatSystem statSystem = StatSystem.getInstance();
	
	public static ArmorPanel getInstance() {
		if (instance == null) {
			instance = new ArmorPanel();
		}
		return instance;
	}
	
	public ArmorPanel() {
		super(new SpringLayout());
		
		anvilImageIcon = new ImageIcon("src/icons/anvil_small.jpg");
		
		ArrayList<String> arrStatTypes = statSystem.getStatTypes();
		statTypes = new String[arrStatTypes.size() + 1];
		statTypes[0] = "None";
		for (int i = 0; i < arrStatTypes.size(); i++) {
			statTypes[i + 1] = arrStatTypes.get(i);
		}
		
		/////////////
		// Content //
		/////////////
		
		// Left Panel
		leftPanel = new JPanel(new SpringLayout());
		leftPane = new JScrollPane(leftPanel);
		leftPane.setPreferredSize(new Dimension(350, 0));
		leftPane.getVerticalScrollBar().setUnitIncrement(16);
		
		int fieldCount = 0;
		JLabel l;
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		entry = new JTextField(15);
		entry.addKeyListener(this);
		l.setLabelFor(entry);
		leftPanel.add(entry);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		name = new JTextField(15);
		name.addKeyListener(this);
		l.setLabelFor(name);
		leftPanel.add(name);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		desc = new JTextField(15);
		desc.addKeyListener(this);
		l.setLabelFor(desc);
		leftPanel.add(desc);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		display = new JTextField(15);
		display.addKeyListener(this);
		l.setLabelFor(display);
		leftPanel.add(display);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		quality = new JComboBox<String>(qualities);
		quality.setSelectedIndex(0);
		l.setLabelFor(quality);
		leftPanel.add(quality);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		subclass = new JComboBox<String>(subclasses);
		subclass.setSelectedIndex(0);
		subclass.addActionListener(this);
		subclass.setActionCommand("subclass");
		l.setLabelFor(subclass);
		leftPanel.add(subclass);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		equip = new JComboBox<String>(miscEquips);
		equip.setSelectedIndex(0);
		l.setLabelFor(equip);
		leftPanel.add(equip);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		bind = new JComboBox<String>(binds);
		bind.setSelectedIndex(0);
		l.setLabelFor(bind);
		leftPanel.add(bind);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		armor = new JTextField(15);
		armor.addKeyListener(this);
		l.setLabelFor(armor);
		leftPanel.add(armor);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		block = new JTextField(15);
		block.addKeyListener(this);
		l.setLabelFor(block);
		leftPanel.add(block);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		duribility = new JTextField(15);
		duribility.addKeyListener(this);
		l.setLabelFor(duribility);
		leftPanel.add(duribility);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		reqlvl = new JTextField(15);
		reqlvl.addKeyListener(this);
		l.setLabelFor(reqlvl);
		leftPanel.add(reqlvl);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		ilvl = new JTextField(15);
		ilvl.addKeyListener(this);
		l.setLabelFor(ilvl);
		leftPanel.add(ilvl);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		unique = new JTextField(15);
		unique.addKeyListener(this);
		l.setLabelFor(unique);
		leftPanel.add(unique);

		/*l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		role = new JComboBox<String>(roles);
		role.setSelectedIndex(0);
		l.setLabelFor(role);
		leftPanel.add(role);*/

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype1 = new JComboBox<String>(statTypes);
		stattype1.setSelectedIndex(0);
		l.setLabelFor(stattype1);
		leftPanel.add(stattype1);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue1 = new JTextField(15);
		statvalue1.addKeyListener(this);
		l.setLabelFor(statvalue1);
		leftPanel.add(statvalue1);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype2 = new JComboBox<String>(statTypes);
		stattype2.setSelectedIndex(0);
		l.setLabelFor(stattype2);
		leftPanel.add(stattype2);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue2 = new JTextField(15);
		statvalue2.addKeyListener(this);
		l.setLabelFor(statvalue2);
		leftPanel.add(statvalue2);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype3 = new JComboBox<String>(statTypes);
		stattype3.setSelectedIndex(0);
		l.setLabelFor(stattype3);
		leftPanel.add(stattype3);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue3 = new JTextField(15);
		statvalue3.addKeyListener(this);
		l.setLabelFor(statvalue3);
		leftPanel.add(statvalue3);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype4 = new JComboBox<String>(statTypes);
		stattype4.setSelectedIndex(0);
		l.setLabelFor(stattype4);
		leftPanel.add(stattype4);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue4 = new JTextField(15);
		statvalue4.addKeyListener(this);
		l.setLabelFor(statvalue4);
		leftPanel.add(statvalue4);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype5 = new JComboBox<String>(statTypes);
		stattype5.setSelectedIndex(0);
		l.setLabelFor(stattype5);
		leftPanel.add(stattype5);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue5 = new JTextField(15);
		statvalue5.addKeyListener(this);
		l.setLabelFor(statvalue5);
		leftPanel.add(statvalue5);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype6 = new JComboBox<String>(statTypes);
		stattype6.setSelectedIndex(0);
		l.setLabelFor(stattype6);
		leftPanel.add(stattype6);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue6 = new JTextField(15);
		statvalue6.addKeyListener(this);
		l.setLabelFor(statvalue6);
		leftPanel.add(statvalue6);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype7 = new JComboBox<String>(statTypes);
		stattype7.setSelectedIndex(0);
		l.setLabelFor(stattype7);
		leftPanel.add(stattype7);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue7 = new JTextField(15);
		statvalue7.addKeyListener(this);
		l.setLabelFor(statvalue7);
		leftPanel.add(statvalue7);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype8 = new JComboBox<String>(statTypes);
		stattype8.setSelectedIndex(0);
		l.setLabelFor(stattype8);
		leftPanel.add(stattype8);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue8 = new JTextField(15);
		statvalue8.addKeyListener(this);
		l.setLabelFor(statvalue8);
		leftPanel.add(statvalue8);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype9 = new JComboBox<String>(statTypes);
		stattype9.setSelectedIndex(0);
		l.setLabelFor(stattype9);
		leftPanel.add(stattype9);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue9 = new JTextField(15);
		statvalue9.addKeyListener(this);
		l.setLabelFor(statvalue9);
		leftPanel.add(statvalue9);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		stattype10 = new JComboBox<String>(statTypes);
		stattype10.setSelectedIndex(0);
		l.setLabelFor(stattype10);
		leftPanel.add(stattype10);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		statvalue10 = new JTextField(15);
		statvalue10.addKeyListener(this);
		l.setLabelFor(statvalue10);
		leftPanel.add(statvalue10);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		holyres = new JTextField(15);
		holyres.addKeyListener(this);
		l.setLabelFor(holyres);
		leftPanel.add(holyres);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		fireres = new JTextField(15);
		fireres.addKeyListener(this);
		l.setLabelFor(fireres);
		leftPanel.add(fireres);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		natureres = new JTextField(15);
		natureres.addKeyListener(this);
		l.setLabelFor(natureres);
		leftPanel.add(natureres);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		frostres = new JTextField(15);
		frostres.addKeyListener(this);
		l.setLabelFor(frostres);
		leftPanel.add(frostres);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		shadowres = new JTextField(15);
		shadowres.addKeyListener(this);
		l.setLabelFor(shadowres);
		leftPanel.add(shadowres);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		arcaneres = new JTextField(15);
		arcaneres.addKeyListener(this);
		l.setLabelFor(arcaneres);
		leftPanel.add(arcaneres);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spell1 = new JTextField(15);
		spell1.addKeyListener(this);
		l.setLabelFor(spell1);
		leftPanel.add(spell1);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spelltrigger1 = new JComboBox<String>(spelltriggers);
		spelltrigger1.setSelectedIndex(0);
		l.setLabelFor(spelltrigger1);
		leftPanel.add(spelltrigger1);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spell2 = new JTextField(15);
		spell2.addKeyListener(this);
		l.setLabelFor(spell2);
		leftPanel.add(spell2);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spelltrigger2 = new JComboBox<String>(spelltriggers);
		spelltrigger2.setSelectedIndex(0);
		l.setLabelFor(spelltrigger2);
		leftPanel.add(spelltrigger2);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spell3 = new JTextField(15);
		spell3.addKeyListener(this);
		l.setLabelFor(spell3);
		leftPanel.add(spell3);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spelltrigger3 = new JComboBox<String>(spelltriggers);
		spelltrigger3.setSelectedIndex(0);
		l.setLabelFor(spelltrigger3);
		leftPanel.add(spelltrigger3);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spell4 = new JTextField(15);
		spell4.addKeyListener(this);
		l.setLabelFor(spell4);
		leftPanel.add(spell4);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spelltrigger4 = new JComboBox<String>(spelltriggers);
		spelltrigger4.setSelectedIndex(0);
		l.setLabelFor(spelltrigger4);
		leftPanel.add(spelltrigger4);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spell5 = new JTextField(15);
		spell5.addKeyListener(this);
		l.setLabelFor(spell5);
		leftPanel.add(spell5);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		spelltrigger5 = new JComboBox<String>(spelltriggers);
		spelltrigger5.setSelectedIndex(0);
		l.setLabelFor(spelltrigger5);
		leftPanel.add(spelltrigger5);

		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		socket1 = new JComboBox<String>(socketColors);
		socket1.setSelectedIndex(0);
		l.setLabelFor(socket1);
		leftPanel.add(socket1);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		socket2 = new JComboBox<String>(socketColors);
		socket2.setSelectedIndex(0);
		l.setLabelFor(socket2);
		leftPanel.add(socket2);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		socket3 = new JComboBox<String>(socketColors);
		socket3.setSelectedIndex(0);
		l.setLabelFor(socket3);
		leftPanel.add(socket3);
		
		l = new JLabel(labels[fieldCount++], JLabel.TRAILING);
		leftPanel.add(l);
		socketbonus = new JTextField(15);
		socketbonus.addKeyListener(this);
		l.setLabelFor(socketbonus);
		leftPanel.add(socketbonus);

		SpringUtilities.makeCompactGrid(leftPanel, fieldCount, 2, 5, 5, 20, 10);
		
		// Right Panel
		rightLayout = new SpringLayout();
		rightPanel = new JPanel(rightLayout);
		rightPane = new JScrollPane(rightPanel);
		// Item Icon
		try {
			itemIcon = new ImageDrawingComponent(new File("src/icons/Inv_misc_questionmark.png").toURI().toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		itemIcon.resize(80, 80);
		SpringLayout.Constraints iconCons = rightLayout.getConstraints(itemIcon);
		iconCons.setX(Spring.constant(120));
		iconCons.setY(Spring.constant(60));
		rightPanel.add(itemIcon);
		// Item ToolTip
		try {
			itemToolTip = new ItemToolTip(item);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		SpringLayout.Constraints toolTipCons = rightLayout.getConstraints(itemToolTip);
		toolTipCons.setX(Spring.constant(210));
		toolTipCons.setY(Spring.constant(60));
		itemToolTip.setVisible(false);
		rightPanel.add(itemToolTip);
		
		invalidDisplayId = new JLabel("Display Id Invalid");
		invalidDisplayId.setFont(italicFont);
		invalidDisplayId.setForeground(Color.red);
		invalidDisplayId.setVisible(false);
		SpringLayout.Constraints invalidDisplayIdCons = rightLayout.getConstraints(invalidDisplayId);
		invalidDisplayIdCons.setX(Spring.constant(90));
		invalidDisplayIdCons.setY(Spring.constant(140));
		rightPanel.add(invalidDisplayId);
		
		createItem = new JButton("Craft", anvilImageIcon);
		createItem.setActionCommand("craft");
		createItem.addActionListener(this);
		SpringLayout.Constraints craftCons = rightLayout.getConstraints(createItem);
		craftCons.setX(Spring.constant(900));
		craftCons.setY(Spring.constant(550));
		rightPanel.add(createItem);
		
		insertItem = new JButton("Insert Into DB");
		insertItem.setActionCommand("insert");
		insertItem.addActionListener(this);
		SpringLayout.Constraints insertCons = rightLayout.getConstraints(insertItem);
		insertCons.setX(Spring.constant(900));
		insertCons.setY(Spring.constant(600));
		rightPanel.add(insertItem);
		
		mainPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPane, rightPane);
		add(mainPane);
		SpringUtilities.makeCompactGrid(this, 1, 1, 6, 6, 6, 6);
	}
	
	/** Returns an ImageIcon, or null if the path was invalid. */
    public static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = ArmorPanel.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /** Fills item class with fields from panel **/
    private void fillItem() {
    	item = new Item();
    	int entryId = 0;
    	if (entry.getText().isEmpty()) {
	    	entryId = Integer.parseInt(SettingsPanel.getInstance().getNextEntryID());
	    	int blockCounter = 0;
	    	while (db.isUsedEntry(entryId)) {
	    		entryId++;
	    		blockCounter++;
	    		// in case it is stuck in a large block, it will run somewhat faster
	    		if (blockCounter == 5) {
	    			entryId += 10;
	    			blockCounter = 1;
	    		}
	    	}
	    	SettingsPanel.getInstance().setNextEntryID(String.valueOf(entryId));
    	} else {
    		try {
    			entryId = Integer.parseInt(entry.getText());
    		} catch (NumberFormatException ex) {
    			JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Entry must be an integer.");
    		}
    	}
    	item.setEntry(entryId);
    	entry.setText(Integer.toString(entryId));
    	
    	// Crafting armor
    	item.setClass_(4);
    	
    	////////////////////////
    	
		if (!display.getText().isEmpty()) {
			String iconPath = "";
			// Do it here in case id exists but just no icon available
			item.setDisplay(Integer.parseInt(display.getText()));
			int displayIdText = 0;
			try {
				displayIdText = Integer.parseInt(display.getText());
			} catch (NumberFormatException ex) {
				// Display Id invalid
				invalidDisplayId.setVisible(true);
				itemIcon.setImage("src/icons/Inv_misc_questionmark.png");
			}
			iconPath = iconFinder.findIconByDisplayId(displayIdText);
			if (iconPath != null && !iconPath.isEmpty()) {
				invalidDisplayId.setVisible(false);
				itemIcon.setImage("src/icons/WoWIcons/" + iconPath + ".png");
				itemIcon.resize(80, 80);
				itemIcon.repaint();
			} else {
				// Display Id invalid
				invalidDisplayId.setVisible(true);
				itemIcon.setImage("src/icons/Inv_misc_questionmark.png");
			}
		}
    	
		if (!name.getText().isEmpty()) {
			item.setName(name.getText());
		}
		
		if (!desc.getText().isEmpty()) {
			item.setDescription(desc.getText());
		}
		
		if (!reqlvl.getText().isEmpty()) {
			item.setReqlvl(Integer.parseInt(reqlvl.getText()));
		}
		
		if (!ilvl.getText().isEmpty()) {
			item.setIlvl(Integer.parseInt(ilvl.getText()));
		}
		
		if (!unique.getText().isEmpty()) {
			item.setUnique(Integer.parseInt(unique.getText()));
		}
		
    	item.setQuality(quality.getSelectedIndex());
    	
    	switch(subclass.getSelectedIndex()) {
    	case 0:
    		// misc
    		item.setSubclass(0);
    		
    		switch (equip.getSelectedIndex()) {
			case 0:
				//neck
				item.setInventoryType(ItemInventoryType.neck.ordinal());
				break;
			case 1:
				//ring
				item.setInventoryType(ItemInventoryType.finger.ordinal());
				break;
			case 2:
				//trinket
				item.setInventoryType(ItemInventoryType.trinket.ordinal());
				break;
			case 3:
				//shirt
				item.setInventoryType(ItemInventoryType.shirt.ordinal());
				break;
			case 4:
				//tabard
				item.setInventoryType(ItemInventoryType.tabard.ordinal());
				break;
			default:
				break;
			}
    		
    		break;
    	case 1:
    		// cloth
    		item.setSubclass(1);
    		
    		switch (equip.getSelectedIndex()) {
			case 0:
				//head
				item.setInventoryType(ItemInventoryType.head.ordinal());
				break;
			case 1:
				//shoulder
				item.setInventoryType(ItemInventoryType.shoulder.ordinal());
				break;
			case 2:
				//cloak
				item.setInventoryType(ItemInventoryType.back.ordinal());
				break;
			case 3:
				//chest
				item.setInventoryType(ItemInventoryType.chest.ordinal());
				break;
			case 4:
				//wrists
				item.setInventoryType(ItemInventoryType.wrists.ordinal());
				break;
			case 5:
				//hands
				item.setInventoryType(ItemInventoryType.hands.ordinal());
				break;
			case 6:
				//waist
				item.setInventoryType(ItemInventoryType.waist.ordinal());
				break;
			case 7:
				//legs
				item.setInventoryType(ItemInventoryType.legs.ordinal());
				break;
			case 8:
				//feet
				item.setInventoryType(ItemInventoryType.feet.ordinal());
				break;
			default:
				break;
			}
    		
    		break;
    	case 2:
    		// leather
    		item.setSubclass(2);
    		
    		switch (equip.getSelectedIndex()) {
			case 0:
				//head
				item.setInventoryType(ItemInventoryType.head.ordinal());
				break;
			case 1:
				//shoulder
				item.setInventoryType(ItemInventoryType.shoulder.ordinal());
				break;
			case 2:
				//chest
				item.setInventoryType(ItemInventoryType.chest.ordinal());
				break;
			case 3:
				//wrists
				item.setInventoryType(ItemInventoryType.wrists.ordinal());
				break;
			case 4:
				//hands
				item.setInventoryType(ItemInventoryType.hands.ordinal());
				break;
			case 5:
				//waist
				item.setInventoryType(ItemInventoryType.waist.ordinal());
				break;
			case 6:
				//legs
				item.setInventoryType(ItemInventoryType.legs.ordinal());
				break;
			case 7:
				//feet
				item.setInventoryType(ItemInventoryType.feet.ordinal());
				break;
			default:
				break;
			}
    		
    		break;
    	case 3:
    		// mail
    		item.setSubclass(3);
    		
    		switch (equip.getSelectedIndex()) {
			case 0:
				//head
				item.setInventoryType(ItemInventoryType.head.ordinal());
				break;
			case 1:
				//shoulder
				item.setInventoryType(ItemInventoryType.shoulder.ordinal());
				break;
			case 2:
				//chest
				item.setInventoryType(ItemInventoryType.chest.ordinal());
				break;
			case 3:
				//wrists
				item.setInventoryType(ItemInventoryType.wrists.ordinal());
				break;
			case 4:
				//hands
				item.setInventoryType(ItemInventoryType.hands.ordinal());
				break;
			case 5:
				//waist
				item.setInventoryType(ItemInventoryType.waist.ordinal());
				break;
			case 6:
				//legs
				item.setInventoryType(ItemInventoryType.legs.ordinal());
				break;
			case 7:
				//feet
				item.setInventoryType(ItemInventoryType.feet.ordinal());
				break;
			default:
				break;
			}
    		
    		break;
    	case 4:
    		// plate
    		item.setSubclass(4);
    		
    		switch (equip.getSelectedIndex()) {
			case 0:
				//head
				item.setInventoryType(ItemInventoryType.head.ordinal());
				break;
			case 1:
				//shoulder
				item.setInventoryType(ItemInventoryType.shoulder.ordinal());
				break;
			case 2:
				//chest
				item.setInventoryType(ItemInventoryType.chest.ordinal());
				break;
			case 3:
				//wrists
				item.setInventoryType(ItemInventoryType.wrists.ordinal());
				break;
			case 4:
				//hands
				item.setInventoryType(ItemInventoryType.hands.ordinal());
				break;
			case 5:
				//waist
				item.setInventoryType(ItemInventoryType.waist.ordinal());
				break;
			case 6:
				//legs
				item.setInventoryType(ItemInventoryType.legs.ordinal());
				break;
			case 7:
				//feet
				item.setInventoryType(ItemInventoryType.feet.ordinal());
				break;
			default:
				break;
			}
    		
    		break;
    	case 5:
    		// shield
    		item.setSubclass(6);
    		item.setInventoryType(ItemInventoryType.shield.ordinal());
    		break;
    	case 6:
    		// libram
    		item.setSubclass(7);
    		item.setInventoryType(ItemInventoryType.relic.ordinal());
    		break;
    	case 7:
    		// idol
    		item.setSubclass(8);
    		item.setInventoryType(ItemInventoryType.relic.ordinal());
    		break;
    	case 8:
    		// totem
    		item.setSubclass(9);
    		item.setInventoryType(ItemInventoryType.relic.ordinal());
    		break;
    	case 9:
    		// sigil
    		item.setSubclass(10);
    		item.setInventoryType(ItemInventoryType.relic.ordinal());
    		break;
    	default:
    		System.err.println("Unknown subclass clicked");
    		break;
    	}
    	
    	item.setBinds(bind.getSelectedIndex());
    	
    	if (!armor.getText().isEmpty())
    		item.setArmor(Integer.parseInt(armor.getText()));
    	if (!block.getText().isEmpty())
    		item.setBlock(Integer.parseInt(block.getText()));
    	if (!duribility.getText().isEmpty())
    		item.setDuribility(Integer.parseInt(duribility.getText()));
    	
    	// Stats
    	int statCount = 0;
    	if (stattype1.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue1.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue1.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue1.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype1.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype2.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue2.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue2.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue2.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype2.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype3.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue3.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue3.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue3.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype3.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype4.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue4.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue4.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue4.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype4.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype5.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue5.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue5.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue5.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype5.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype6.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue6.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue6.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue6.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype6.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype7.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue7.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue7.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue7.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype7.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype8.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue8.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue8.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue8.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype8.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype9.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue9.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue9.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue9.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype9.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	if (stattype10.getSelectedIndex() > 0) {
    		int statValue = 0;
    		if (!statvalue10.getText().isEmpty()) {
    			try {
    				statValue = Integer.parseInt(statvalue10.getText());
    			} catch (NumberFormatException ne) {
    				// invalid integer
    				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for stat " + statCount);
    			}
    		} else {
    			statValue = calcStat();
    		}
    		statvalue10.setText(Integer.toString(statValue));
    		item.setStat_type(statCount, statSystem.getStatId((String)stattype10.getSelectedItem()));
    		item.setStat_value(statCount, statValue);
    		statCount++;
    	}
    	
    	item.setStatsCount(statCount);
    	
    	// resists
    	try {
	    	item.setHoly_resist(Integer.parseInt(holyres.getText()));
	    	item.setFire_resist(Integer.parseInt(fireres.getText()));
	    	item.setNature_resist(Integer.parseInt(natureres.getText()));
	    	item.setFrost_resist(Integer.parseInt(frostres.getText()));
	    	item.setShadow_resist(Integer.parseInt(shadowres.getText()));
	    	item.setArcane_resist(Integer.parseInt(arcaneres.getText()));
    	} catch (NumberFormatException ne) {
    		JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid value for resist");
    	}
    	   	
    	// Spells
    	if (!spell1.getText().isEmpty()) {
    		int spellindex = 0;
    		String spellDesc = spellFinder.findSpellDescriptionById(Integer.parseInt(spell1.getText()));
    		String spellIcon = spellFinder.findSpellIconById(Integer.parseInt(spell1.getText()));
    		item.setSpell_id(spellindex, Integer.parseInt(spell1.getText()));
    		item.setSpell_trigger(spellindex, spelltrigger1.getSelectedIndex());
    	}
    	if (!spell2.getText().isEmpty()) {
    		int spellindex = 1;
    		String spellDesc = spellFinder.findSpellDescriptionById(Integer.parseInt(spell2.getText()));
    		String spellIcon = spellFinder.findSpellIconById(Integer.parseInt(spell2.getText()));
    		item.setSpell_id(1, Integer.parseInt(spell2.getText()));
    		item.setSpell_trigger(spellindex, spelltrigger2.getSelectedIndex());
    	}
    	if (!spell3.getText().isEmpty()) {
    		int spellindex = 2;
    		String spellDesc = spellFinder.findSpellDescriptionById(Integer.parseInt(spell3.getText()));
    		String spellIcon = spellFinder.findSpellIconById(Integer.parseInt(spell3.getText()));
    		item.setSpell_id(2, Integer.parseInt(spell3.getText()));
    		item.setSpell_trigger(spellindex, spelltrigger3.getSelectedIndex());
    	}
    	if (!spell4.getText().isEmpty()) {
    		int spellindex = 3;
    		String spellDesc = spellFinder.findSpellDescriptionById(Integer.parseInt(spell4.getText()));
    		String spellIcon = spellFinder.findSpellIconById(Integer.parseInt(spell4.getText()));
    		item.setSpell_id(3, Integer.parseInt(spell4.getText()));
    		item.setSpell_trigger(spellindex, spelltrigger4.getSelectedIndex());
    	}
    	if (!spell5.getText().isEmpty()) {
    		int spellindex = 4;
    		String spellDesc = spellFinder.findSpellDescriptionById(Integer.parseInt(spell5.getText()));
    		String spellIcon = spellFinder.findSpellIconById(Integer.parseInt(spell5.getText()));
    		item.setSpell_id(4, Integer.parseInt(spell5.getText()));
    		item.setSpell_trigger(spellindex, spelltrigger5.getSelectedIndex());
    	}
    	
    	// Sockets
    	if (socket1.getSelectedIndex() > 0) {
    		if (socket1.getSelectedItem().equals("Meta")) {
    			item.setSocket_color(0, 1);
    		} else if (socket1.getSelectedItem().equals("Red")) {
    			item.setSocket_color(0, 2);
    		} else if (socket1.getSelectedItem().equals("Yellow")) {
    			item.setSocket_color(0, 4);
    		} else if (socket1.getSelectedItem().equals("Blue")) {
    			item.setSocket_color(0, 8);
    		} else if (socket1.getSelectedItem().equals("Prismatic")) {
    			item.setSocket_color(0, 14);
    		}
    	}
    	if (socket2.getSelectedIndex() > 0) {
    		if (socket2.getSelectedItem().equals("Meta")) {
    			item.setSocket_color(1, 1);
    		} else if (socket2.getSelectedItem().equals("Red")) {
    			item.setSocket_color(1, 2);
    		} else if (socket2.getSelectedItem().equals("Yellow")) {
    			item.setSocket_color(1, 4);
    		} else if (socket2.getSelectedItem().equals("Blue")) {
    			item.setSocket_color(1, 8);
    		} else if (socket2.getSelectedItem().equals("Prismatic")) {
    			item.setSocket_color(1, 14);
    		}
    	}
    	if (socket3.getSelectedIndex() > 0) {
    		if (socket3.getSelectedItem().equals("Meta")) {
    			item.setSocket_color(2, 1);
    		} else if (socket3.getSelectedItem().equals("Red")) {
    			item.setSocket_color(2, 2);
    		} else if (socket3.getSelectedItem().equals("Yellow")) {
    			item.setSocket_color(2, 4);
    		} else if (socket3.getSelectedItem().equals("Blue")) {
    			item.setSocket_color(2, 8);
    		} else if (socket3.getSelectedItem().equals("Prismatic")) {
    			item.setSocket_color(2, 14);
    		}
    	}
    	
    	try {
    		item.setSocketBonus(Integer.parseInt(socketbonus.getText()));
    	} catch (NumberFormatException ne) {
    		JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Invalid integer for socket bonus");
    	}
    	
    	//repaint
    	rightPanel.remove(itemToolTip);
    	try {
			itemToolTip = new ItemToolTip(item);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		SpringLayout.Constraints toolTipCons = rightLayout.getConstraints(itemToolTip);
		toolTipCons.setX(Spring.constant(210));
		toolTipCons.setY(Spring.constant(60));
		rightPanel.add(itemToolTip);
		
		rightPanel.setPreferredSize(new Dimension(850, itemToolTip.getToolTipHeight()+125));
		
		repaint();
		revalidate();
    }
    
    public void setCurrentItem(Item item) {
    	this.item = item;
    	if (item != null)
    		fillCurrentItemFields();
    }
    
    public void fillCurrentItemFields() {
    	// clear fields
    	clearFields();
    	
    	// Start filling fields from item
    	entry.setText(Integer.toString(item.getEntry()));
    	name.setText(item.getName());
    	desc.setText(item.getDescription());
    	display.setText(Integer.toString(item.getDisplay()));
    	quality.setSelectedIndex(item.getQuality());
    	
    	switch(item.getSubclass()) {
    	case 0:
    		// misc
    		subclass.setSelectedIndex(0);
    		
    		setMiscEquip();
    		switch (item.getInventoryType()) {
    		case 2:
    			equip.setSelectedIndex(0);
    			break;
    		case 11:
    			equip.setSelectedIndex(1);
    			break;
    		case 12:
    			equip.setSelectedIndex(2);
    			break;
    		case 4:
    			equip.setSelectedIndex(3);
    			break;
    		case 19:
    			equip.setSelectedIndex(4);
    			break;
    		default:
    			break;
    		}
    		
    		break;
    	case 1:
    		// cloth
    		subclass.setSelectedIndex(1);
    		
    		setClothEquip();
    		switch (item.getInventoryType()) {
    		case 1:
    			equip.setSelectedIndex(0);
    			break;
    		case 3:
    			equip.setSelectedIndex(1);
    			break;
    		case 16:
    			equip.setSelectedIndex(2);
    			break;
    		case 5:
    			equip.setSelectedIndex(3);
    			break;
    		case 9:
    			equip.setSelectedIndex(4);
    			break;
    		case 10:
    			equip.setSelectedIndex(5);
    			break;
    		case 6:
    			equip.setSelectedIndex(6);
    			break;
    		case 7:
    			equip.setSelectedIndex(7);
    			break;
    		case 8:
    			equip.setSelectedIndex(8);
    			break;
    		default:
    			break;
    		}
    		
    		break;
    	case 2:
    		// leather
    		subclass.setSelectedIndex(2);
    		
    		setMainEquip();
    		switch (item.getInventoryType()) {
    		case 1:
    			equip.setSelectedIndex(0);
    			break;
    		case 3:
    			equip.setSelectedIndex(1);
    			break;
    		case 5:
    			equip.setSelectedIndex(2);
    			break;
    		case 9:
    			equip.setSelectedIndex(3);
    			break;
    		case 10:
    			equip.setSelectedIndex(4);
    			break;
    		case 6:
    			equip.setSelectedIndex(5);
    			break;
    		case 7:
    			equip.setSelectedIndex(6);
    			break;
    		case 8:
    			equip.setSelectedIndex(7);
    			break;
    		default:
    			break;
    		}
    		
    		break;
    	case 3:
    		// mail
    		subclass.setSelectedIndex(3);
    		
    		setMainEquip();
    		switch (item.getInventoryType()) {
    		case 1:
    			equip.setSelectedIndex(0);
    			break;
    		case 3:
    			equip.setSelectedIndex(1);
    			break;
    		case 5:
    			equip.setSelectedIndex(2);
    			break;
    		case 9:
    			equip.setSelectedIndex(3);
    			break;
    		case 10:
    			equip.setSelectedIndex(4);
    			break;
    		case 6:
    			equip.setSelectedIndex(5);
    			break;
    		case 7:
    			equip.setSelectedIndex(6);
    			break;
    		case 8:
    			equip.setSelectedIndex(7);
    			break;
    		default:
    			break;
    		}
    		
    		break;
    	case 4:
    		// plate
    		subclass.setSelectedIndex(4);
    		
    		setMainEquip();
    		switch (item.getInventoryType()) {
    		case 1:
    			equip.setSelectedIndex(0);
    			break;
    		case 3:
    			equip.setSelectedIndex(1);
    			break;
    		case 5:
    			equip.setSelectedIndex(2);
    			break;
    		case 9:
    			equip.setSelectedIndex(3);
    			break;
    		case 10:
    			equip.setSelectedIndex(4);
    			break;
    		case 6:
    			equip.setSelectedIndex(5);
    			break;
    		case 7:
    			equip.setSelectedIndex(6);
    			break;
    		case 8:
    			equip.setSelectedIndex(7);
    			break;
    		default:
    			break;
    		}
    		
    		break;
    	case 6:
    		// shield
    		subclass.setSelectedIndex(5);
    		setShieldEquip();
    		equip.setSelectedIndex(0);
    		break;
    	case 7:
    		// libram
    		subclass.setSelectedIndex(6);
    		setRelicEquip();
    		equip.setSelectedIndex(0);
    		break;
    	case 8:
    		// idol
    		subclass.setSelectedIndex(7);
    		setRelicEquip();
    		equip.setSelectedIndex(0);
    		break;
    	case 9:
    		// totem
    		subclass.setSelectedIndex(8);
    		setRelicEquip();
    		equip.setSelectedIndex(0);
    		break;
    	case 10:
    		// sigil
    		subclass.setSelectedIndex(9);
    		setRelicEquip();
    		equip.setSelectedIndex(0);
    		break;
    	default:
    		System.err.println("Unknown Item Subclass");
    		break;
    	}
    	
    	bind.setSelectedIndex(item.getBinds());
    	
    	if (item.getArmor() != 0)
    		armor.setText(Integer.toString(item.getArmor()));
    	if (item.getBlock() != 0)
    		block.setText(Integer.toString(item.getBlock()));
    	if (item.getDuribility() != 0)
    		duribility.setText(Integer.toString(item.getDuribility()));
    	if (item.getReqlvl() != 0)
    		reqlvl.setText(Integer.toString(item.getReqlvl()));
    	if (item.getIlvl() != 0)
    		ilvl.setText(Integer.toString(item.getIlvl()));
    	if (item.getUnique() != 0)
    		unique.setText(Integer.toString(item.getUnique()));
    	
    	int[] selectedStatIndices = new int[item.getStatsCount()];
    	int selectedSCounter = 0;
    	for (int i = 0; i < item.getStatsCount(); i++) {
    		if (item.getStat_value(i) != 0) {
    			switch(item.getStat_type(i)) {
    			case 3:
    				selectedStatIndices[selectedSCounter] = 0;
    				selectedSCounter++;
    				break;
    			case 4:
    				selectedStatIndices[selectedSCounter] = 2;
    				selectedSCounter++;
    				break;
    			case 5:
    				selectedStatIndices[selectedSCounter] = 3;
    				selectedSCounter++;
    				break;
    			case 6:
    				selectedStatIndices[selectedSCounter] = 4;
    				selectedSCounter++;
    				break;
    			case 7:
    				selectedStatIndices[selectedSCounter] = 1;
    				selectedSCounter++;
    				break;
    			case 12:
    				selectedStatIndices[selectedSCounter] = 13;
    				selectedSCounter++;
    				break;
    			case 13:
    				selectedStatIndices[selectedSCounter] = 14;
    				selectedSCounter++;
    				break;
    			case 14:
    				selectedStatIndices[selectedSCounter] = 15;
    				selectedSCounter++;
    				break;
    			case 15:
    				selectedStatIndices[selectedSCounter] = 16;
    				selectedSCounter++;
    				break;
    			case 31:
    				selectedStatIndices[selectedSCounter] = 7;
    				selectedSCounter++;
    				break;
    			case 32:
    				selectedStatIndices[selectedSCounter] = 8;
    				selectedSCounter++;
    				break;
    			case 35:
    				selectedStatIndices[selectedSCounter] = 17;
    				selectedSCounter++;
    				break;
    			case 36:
    				selectedStatIndices[selectedSCounter] = 9;
    				selectedSCounter++;
    				break;
    			case 37:
    				selectedStatIndices[selectedSCounter] = 12;
    				selectedSCounter++;
    				break;
    			case 38:
    				selectedStatIndices[selectedSCounter] = 6;
    				selectedSCounter++;
    				break;
    			case 44:
    				selectedStatIndices[selectedSCounter] = 10;
    				selectedSCounter++;
    				break;
    			case 45:
    				selectedStatIndices[selectedSCounter] = 5;
    				selectedSCounter++;
    				break;
    			case 47:
    				selectedStatIndices[selectedSCounter] = 11;
    				selectedSCounter++;
    				break;
    			default:
    				System.err.println("Stat that could not be selected was loaded to armor panel.");
    				// show error
    				break;
    			}
    		}
    	}
    	
    	// stats
    	stattype1.setSelectedItem(statSystem.getStatType(item.getStat_type(0)));
    	statvalue1.setText(Integer.toString(item.getStat_value(0)));
    	stattype2.setSelectedItem(statSystem.getStatType(item.getStat_type(1)));
    	statvalue2.setText(Integer.toString(item.getStat_value(1)));
    	stattype3.setSelectedItem(statSystem.getStatType(item.getStat_type(2)));
    	statvalue3.setText(Integer.toString(item.getStat_value(2)));
    	stattype4.setSelectedItem(statSystem.getStatType(item.getStat_type(3)));
    	statvalue4.setText(Integer.toString(item.getStat_value(3)));
    	stattype5.setSelectedItem(statSystem.getStatType(item.getStat_type(4)));
    	statvalue5.setText(Integer.toString(item.getStat_value(4)));
    	stattype6.setSelectedItem(statSystem.getStatType(item.getStat_type(5)));
    	statvalue6.setText(Integer.toString(item.getStat_value(5)));
    	stattype7.setSelectedItem(statSystem.getStatType(item.getStat_type(6)));
    	statvalue7.setText(Integer.toString(item.getStat_value(6)));
    	stattype8.setSelectedItem(statSystem.getStatType(item.getStat_type(7)));
    	statvalue8.setText(Integer.toString(item.getStat_value(7)));
    	stattype9.setSelectedItem(statSystem.getStatType(item.getStat_type(8)));
    	statvalue9.setText(Integer.toString(item.getStat_value(8)));
    	stattype10.setSelectedItem(statSystem.getStatType(item.getStat_type(9)));
    	statvalue10.setText(Integer.toString(item.getStat_value(9)));
    	
    	holyres.setText(Integer.toString(item.getHoly_resist()));
    	fireres.setText(Integer.toString(item.getFire_resist()));
    	natureres.setText(Integer.toString(item.getNature_resist()));
    	frostres.setText(Integer.toString(item.getFrost_resist()));
    	shadowres.setText(Integer.toString(item.getShadow_resist()));
    	arcaneres.setText(Integer.toString(item.getArcane_resist()));
    	
    	if (item.getSpell_id(0) != 0) {
    		spell1.setText(Integer.toString(item.getSpell_id(0)));
    		spelltrigger1.setSelectedIndex(item.getSpell_trigger(0));
    	}
    	if (item.getSpell_id(1) != 0) {
	    	spell2.setText(Integer.toString(item.getSpell_id(1)));
	    	spelltrigger2.setSelectedIndex(item.getSpell_trigger(1));
    	}
    	if (item.getSpell_id(2) != 0) {
	    	spell3.setText(Integer.toString(item.getSpell_id(2)));
	    	spelltrigger3.setSelectedIndex(item.getSpell_trigger(2));
    	}
    	if (item.getSpell_id(3) != 0) {
	    	spell4.setText(Integer.toString(item.getSpell_id(3)));
	    	spelltrigger4.setSelectedIndex(item.getSpell_trigger(3));
    	}
    	if (item.getSpell_id(4) != 0) {
	    	spell5.setText(Integer.toString(item.getSpell_id(4)));
	    	spelltrigger5.setSelectedIndex(item.getSpell_trigger(4));
    	}
    	
    	//sockets
    	switch (item.getSocket_color(0)) {
    	case 1:
    		socket1.setSelectedItem("Meta");
    		break;
    	case 2:
    		socket1.setSelectedItem("Red");
    		break;
    	case 4:
    		socket1.setSelectedItem("Yellow");
    		break;
    	case 8:
    		socket1.setSelectedItem("Blue");
    		break;
    	case 14:
    		socket1.setSelectedItem("Prismatic");
    		break;
    	}
    	
    	switch (item.getSocket_color(1)) {
    	case 1:
    		socket2.setSelectedItem("Meta");
    		break;
    	case 2:
    		socket2.setSelectedItem("Red");
    		break;
    	case 4:
    		socket2.setSelectedItem("Yellow");
    		break;
    	case 8:
    		socket2.setSelectedItem("Blue");
    		break;
    	case 14:
    		socket2.setSelectedItem("Prismatic");
    		break;
    	}
    	
    	switch (item.getSocket_color(2)) {
    	case 1:
    		socket3.setSelectedItem("Meta");
    		break;
    	case 2:
    		socket3.setSelectedItem("Red");
    		break;
    	case 4:
    		socket3.setSelectedItem("Yellow");
    		break;
    	case 8:
    		socket3.setSelectedItem("Blue");
    		break;
    	case 14:
    		socket3.setSelectedItem("Prismatic");
    		break;
    	}
    	
    	socketbonus.setText(Integer.toString(item.getSocketBonus()));
    	
    	// Repaint Icon
    	if (!display.getText().isEmpty()) {
			String iconPath = "";
			// Do it here in case id exists but just no icon available
			item.setDisplay(Integer.parseInt(display.getText()));
			int displayIdText = 0;
			try {
				displayIdText = Integer.parseInt(display.getText());
			} catch (NumberFormatException ex) {
				// Display Id invalid
				invalidDisplayId.setVisible(true);
				itemIcon.setImage("src/icons/Inv_misc_questionmark.png");
			}
			iconPath = iconFinder.findIconByDisplayId(displayIdText);
			if (iconPath != null && !iconPath.isEmpty()) {
				invalidDisplayId.setVisible(false);
				itemIcon.setImage("src/icons/WoWIcons/" + iconPath + ".png");
				itemIcon.resize(80, 80);
				itemIcon.repaint();
			} else {
				// Display Id invalid
				invalidDisplayId.setVisible(true);
				itemIcon.setImage("src/icons/Inv_misc_questionmark.png");
			}
		}
    	
    	//Repaint ToolTip
    	rightPanel.remove(itemToolTip);
    	try {
			itemToolTip = new ItemToolTip(item);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		SpringLayout.Constraints toolTipCons = rightLayout.getConstraints(itemToolTip);
		toolTipCons.setX(Spring.constant(210));
		toolTipCons.setY(Spring.constant(60));
		rightPanel.add(itemToolTip);
		
		rightPanel.setPreferredSize(new Dimension(850, itemToolTip.getToolTipHeight()+125));
		
		repaint();
		revalidate();
    }
    
    private void clearFields() {
    	entry.setText("");
    	name.setText("");
    	desc.setText("");
    	display.setText("");
    	quality.setSelectedIndex(0);
    	subclass.setSelectedIndex(0);
    	setMiscEquip();
    	equip.setSelectedIndex(0);
    	bind.setSelectedIndex(0);
    	armor.setText("");
    	block.setText("");
    	duribility.setText("");
    	reqlvl.setText("");
    	ilvl.setText("");
    	unique.setText("");
    	stattype1.setSelectedIndex(0);
    	statvalue1.setText("");
    	stattype2.setSelectedIndex(0);
    	statvalue2.setText("");
    	stattype3.setSelectedIndex(0);
    	statvalue3.setText("");
    	stattype4.setSelectedIndex(0);
    	statvalue4.setText("");
    	stattype5.setSelectedIndex(0);
    	statvalue5.setText("");
    	stattype6.setSelectedIndex(0);
    	statvalue6.setText("");
    	stattype7.setSelectedIndex(0);
    	statvalue7.setText("");
    	stattype8.setSelectedIndex(0);
    	statvalue8.setText("");
    	stattype9.setSelectedIndex(0);
    	statvalue9.setText("");
    	stattype10.setSelectedIndex(0);
    	statvalue10.setText("");
    	holyres.setText("");
    	fireres.setText("");
    	natureres.setText("");
    	frostres.setText("");
    	shadowres.setText("");
    	arcaneres.setText("");
    	spell1.setText("");
    	spelltrigger1.setSelectedIndex(0);
    	spell2.setText("");
    	spelltrigger2.setSelectedIndex(0);
    	spell3.setText("");
    	spelltrigger3.setSelectedIndex(0);
    	spell4.setText("");
    	spelltrigger4.setSelectedIndex(0);
    	spell5.setText("");
    	spelltrigger5.setSelectedIndex(0);
    	socket1.setSelectedIndex(0);
    	socket2.setSelectedIndex(0);
    	socket3.setSelectedIndex(0);
    	socketbonus.setText("");
    }
    
    private void setMiscEquip() {
    	equip.removeAllItems();
		for (int i = 0; i < miscEquips.length; i++) {
			equip.addItem(miscEquips[i]);
		}
    }
    
    private void setClothEquip() {
    	equip.removeAllItems();
		for (int i = 0; i < clothEquips.length; i++) {
			equip.addItem(clothEquips[i]);
		}
    }
    
    private void setMainEquip() {
    	equip.removeAllItems();
		for (int i = 0; i < mainEquips.length; i++) {
			equip.addItem(mainEquips[i]);
		}
    }
    
    private void setShieldEquip() {
    	equip.removeAllItems();
		for (int i = 0; i < shieldEquip.length; i++) {
			equip.addItem(shieldEquip[i]);
		}
    }
    
    private void setRelicEquip() {
    	equip.removeAllItems();
		for (int i = 0; i < relicEquip.length; i++) {
			equip.addItem(relicEquip[i]);
		}
    }
    
	@Override
	public void focusGained(FocusEvent e) {}

	@Override
	public void focusLost(FocusEvent e) {}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(createItem.getActionCommand())) {
			// Craft button clicked
			if (!name.getText().isEmpty()) {
				fillItem();
			} else {
				// name field is empty
				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "Give your item a name.");
			}
		} else if (e.getActionCommand().equals(insertItem.getActionCommand())) {
			if (item.getEntry() != 0) {
				final JOptionPane optionPane = new JOptionPane(
		                "Insert item into Database?",
		                JOptionPane.QUESTION_MESSAGE,
		                JOptionPane.YES_NO_OPTION);
	
				final JDialog dialog = new JDialog(ItemBuilder.getInstance().getIBFrame(), 
				                             "Database Confirmation",
				                             true);
				dialog.setContentPane(optionPane);
				
				dialog.setLocation(dialog.getParent().getX()+650, dialog.getParent().getY()+350);
				optionPane.addPropertyChangeListener(
				    new PropertyChangeListener() {
				        public void propertyChange(PropertyChangeEvent e) {
				            String prop = e.getPropertyName();
		
				            if (dialog.isVisible() 
				             && (e.getSource() == optionPane)
				             && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
				                //If you were going to check something
				                //before closing the window, you'd do
				                //it here.
				                dialog.setVisible(false);
				            }
				        }
				    });
				dialog.pack();
				dialog.setVisible(true);
		
				int value;
				try {
					value = ((Integer)optionPane.getValue()).intValue();
				} catch(Exception e2) {
					// closed the window
					value = JOptionPane.NO_OPTION;
				}
				if (value == JOptionPane.YES_OPTION) {
					System.out.println("Inserting Item into DB.");
					try {
						db.insertItemIntoTemplate(item);
						int nextEntry = item.getEntry() + 1;
						SettingsPanel.getInstance().setNextEntryID(String.valueOf(nextEntry));
					} catch (Exception e1) {
						System.err.println("Failed to insert item into db.");
						e1.printStackTrace();
					}
				} else if (value == JOptionPane.NO_OPTION) {
				    System.out.println("Not inserting item into DB.");
				}
			} else {
				// entry == 0
				// No item to insert
				JOptionPane.showMessageDialog(ItemBuilder.getInstance().getIBFrame(), "You must craft an item first.");
			}
		} else if (e.getActionCommand().equals(subclass.getActionCommand())) {
			switch (subclass.getSelectedIndex()) {
			case 0:
				// misc
				setMiscEquip();
				break;
			case 1:
				// cloth
				setClothEquip();
				break;
			case 2:
			case 3:
			case 4:
				// leather, mail, plate
				setMainEquip();
				break;
			case 5:
				//shield
				setShieldEquip();
				break;
			case 6:
			case 7:
			case 8:
			case 9:
				// librams, idols, totems, sigils
				setRelicEquip();
				break;
			default:
				System.err.println("Unknown subclass clicked");
				break;
			}
		}
	}
    
    @Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			createItem.doClick();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	
	// this needs to be implemented
		private int calcStat() {
			/*String statText = "";
			// initialize true because their are fewer normal stats
			boolean isEquipStat = true;
			float statMultiplier = 1;
			switch(statsToCalc[i]) {
			case 0: //agility
				item.setStat_type(i, 3);
				statMultiplier = 1.26f;
				statText = " Agility";
				isEquipStat = false;
				break;
			case 1: //stamina
				item.setStat_type(i, 7);
				statMultiplier = 1.75f;
				statText = " Stamina";
				isEquipStat = false;
				break;
			case 2: //strength
				item.setStat_type(i, 4);
				statMultiplier = 1.45f;
				statText = " Strength";
				isEquipStat = false;
				break;
			case 3: //intellect
				item.setStat_type(i, 5);
				statMultiplier = 1.49f;
				statText = " Intellect";
				isEquipStat = false;
			    break;
			case 4: //spirit
				item.setStat_type(i, 6);
				statMultiplier = 1.85f;
				statText = " Spirit";
				isEquipStat = false;
				break;
			case 5: //spell power
				item.setStat_type(i, 45);
				statMultiplier = 2.72f;
				statText = "Equip: Increases spell power by ";
				break;
			case 6: //attack power
				item.setStat_type(i, 38);
				statMultiplier = 2.61f;
				statText = "Equip: Increases attack power by ";
				break;
			case 7: //hitrating
				item.setStat_type(i, 31);
				statMultiplier = 0.65f;
				statText = "Equip: Improves hit rating by ";
				break;
			case 8: //critrating
				item.setStat_type(i, 32);
				statMultiplier = 0.75f;
				statText = "Equip: Improves critical strike rating by ";
				break;
			case 9: //hasterating
				item.setStat_type(i, 36);
				statMultiplier = 0.5f;
				statText = "Equip: Improves haste rating by ";
				break;
			case 10: //armorpen
				item.setStat_type(i, 44);
				statMultiplier = 0.85f;
				statText = "Equip: Improves armor penetration by ";
				break;
			case 11: //spellpen
				item.setStat_type(i, 47);
				statMultiplier = 0.85f;
				statText = "Equip: Improves spell penetration by ";
				break;
			case 12: //expertise
				item.setStat_type(i, 37);
				statMultiplier = 1.05f;
				statText = "Equip: Improves expertise rating by ";
				break;
			case 13: //defense
				item.setStat_type(i, 12);
				statMultiplier = 1.36f;
				statText = "Equip: Improves defense rating by ";
				break;
			case 14: //dodge
				item.setStat_type(i, 13);
				statMultiplier = 1.49f;
				statText = "Equip: Improves dodge rating by ";
				break;
			case 15: //parry
				item.setStat_type(i, 14);
				statMultiplier = 1.52f;
				statText = "Equip: Improves parry rating by ";
				break;
			case 16: //resilience
				item.setStat_type(i, 35);
				statMultiplier = 0.95f;
				statText = "Equip: Improves your resilience rating by ";
				break;
			default: //nothing
				item.setStat_type(i, 0);
				// No stats should be added to item
				statText = "";
				statMultiplier = 0;
				break;
			}
			
			double subclassMultiplier = 1;
			
			switch (item.getSubclass()) {
			// One handers
			case 0:
			case 4:
			case 7:
			case 13:
			case 15:
				subclassMultiplier = Double.parseDouble(serverConfigPanel.getOneHandStatMultiplier());
				break;
			// ranged
			case 2:
			case 3:
			case 16:
			case 17:
			case 18:
			case 19:
				subclassMultiplier = Double.parseDouble(serverConfigPanel.getRangedStatMultiplier());
				break;
			// Two handers
			case 1:
			case 5:
			case 6:
			case 8:
			case 10:
				subclassMultiplier = Double.parseDouble(serverConfigPanel.getTwoHandStatMultiplier());
				break;
			default:
				break;
			}
			// TODO: Make this serverMultiplier an option
			double serverWeaponMultiplier = Double.parseDouble(serverConfigPanel.getWeaponStatMultiplier());
			// Do calculation for value based off of quality and ilvl and subclass
			double statValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/4;
			item.setStat_value(i, (int)statValue);*/
			return 0;
		}
		
		private int calcResist() {
			/*int restistToCalc[] = selectedResists.getSelectedIndices();
	    	float resistValue;
	    	
	    	if (restistToCalc.length > 6) {
	    		// print error message, can only have 10 stats
	    	}
	    	
	    	for (int i = 0; i < restistToCalc.length && i < 6; i++) {
	    		String resistText = "";
	    		// initialize true because their are less normal stats
	    		float statMultiplier = 1;
	    		float subclassMultiplier = 1;
	    		
	    		switch (item.getSubclass()) {
	    		// One handers
	    		case 0:
	    		case 4:
	    		case 7:
	    		case 13:
	    		case 15:
	    			subclassMultiplier = Float.parseFloat(serverConfigPanel.getOneHandStatMultiplier());
	    			break;
	    		// ranged
	    		case 2:
	    		case 3:
	    		case 16:
	    		case 17:
	    		case 18:
	    		case 19:
	    			subclassMultiplier = Float.parseFloat(serverConfigPanel.getRangedStatMultiplier());
	    			break;
	    		// Two handers
	    		case 1:
	    		case 5:
	    		case 6:
	    		case 8:
	    		case 10:
	    			subclassMultiplier = Float.parseFloat(serverConfigPanel.getTwoHandStatMultiplier());
	    			break;
	    		default:
	    			break;
	    		}
	    		
	    		// TODO: Make this serverMultiplier an option
	    		float serverWeaponMultiplier = Float.parseFloat(serverConfigPanel.getWeaponStatMultiplier());
	    		// Do calculation for value based off of quality and ilvl and subclass
	    		
	    		switch(restistToCalc[i]) {
	    		case 0: //fire
	    			resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
	    			if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases fire resist by ";
	    			item.setFire_resist((int)resistValue);
	    			break;
	    		case 1: //frost
	    			resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
	    			if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases frost resist by ";
	    			item.setFrost_resist((int)resistValue);
	    			break;
	    		case 2: //shadow
	    			resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
	    			if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases shadow resist by ";
	    			item.setShadow_resist((int)resistValue);
	    			break;
				case 3: //holy
					resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
					if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases holy resist by ";
	    			item.setHoly_resist((int)resistValue);
				    break;
				case 4: //nature
					resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
					if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases nature resist by ";
	    			item.setNature_resist((int)resistValue);
					break;
				case 5: //arcane
					resistValue = serverWeaponMultiplier*subclassMultiplier*statMultiplier*item.getIlvl()*item.getQuality()/12;
					if (resistValue > 254)
	    				resistValue = (float) (240 + (Math.random() * 14));
	    			resistText = "Equip: Increases nature resist by ";
	    			item.setArcane_resist((int)resistValue);
					break;
				default: //nothing
	    			// No resists should be added to item
	    			break;
	    		}
	    		
	    	}*/
			return 0;
		}
}
