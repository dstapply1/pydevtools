package PYDevTools.db.item;

import java.util.ArrayList;

public class StatSystem {
	private static StatSystem instance = null;
	
	private ArrayList<Stat> stats;
	
	private StatSystem() {
		stats = new ArrayList<Stat>();
		
		stats.add(new Stat(0, "Mana"));
		stats.add(new Stat(1, "Health"));
		stats.add(new Stat(3, "Agility"));
		stats.add(new Stat(4, "Strength"));
		stats.add(new Stat(5, "Intellect"));
		stats.add(new Stat(6, "Spirit"));
		stats.add(new Stat(7, "Stamina"));
		stats.add(new Stat(12, "Defense Rating"));
		stats.add(new Stat(13, "Dodge Rating"));
		stats.add(new Stat(14, "Parry Rating"));
		stats.add(new Stat(15, "Block Rating"));
		stats.add(new Stat(16, "Melee Hit Rating"));
		stats.add(new Stat(17, "Ranged Hit Rating"));
		stats.add(new Stat(18, "Spell Hit Rating"));
		stats.add(new Stat(19, "Melee Crit Rating"));
		stats.add(new Stat(20, "Ranged Crit Rating"));
		stats.add(new Stat(21, "Spell Crit Rating"));
		stats.add(new Stat(22, "Melee Hit Taken Rating"));
		stats.add(new Stat(23, "Ranged Hit Taken Rating"));
		stats.add(new Stat(24, "Spell Hit Taken Rating"));
		stats.add(new Stat(25, "Melee Crit Taken Rating"));
		stats.add(new Stat(26, "Ranged Crit Taken Rating"));
		stats.add(new Stat(27, "Spell Crit Taken Rating"));
		stats.add(new Stat(28, "Melee Haste Rating"));
		stats.add(new Stat(29, "Ranged Haste Rating"));
		stats.add(new Stat(30, "Spell Haste Rating"));
		stats.add(new Stat(31, "Hit Rating"));
		stats.add(new Stat(32, "Crit Rating"));
		stats.add(new Stat(33, "Hit Taken Rating"));
		stats.add(new Stat(34, "Crit Taken Rating"));
		stats.add(new Stat(35, "Resilience Rating"));
		stats.add(new Stat(36, "Haste Rating"));
		stats.add(new Stat(37, "Expertise Rating"));
		stats.add(new Stat(38, "Attack Power"));
		stats.add(new Stat(39, "Ranged Attack Power"));
		stats.add(new Stat(40, "Feral Attack Power"));
		stats.add(new Stat(41, "Spell Healing Done"));
		stats.add(new Stat(42, "Spell Damage Done"));
		stats.add(new Stat(43, "Mana Regeneration"));
		stats.add(new Stat(44, "Armor Penetration"));
		stats.add(new Stat(45, "Spell Power"));
		stats.add(new Stat(46, "Health Regeneration"));
		stats.add(new Stat(47, "Spell Penetration"));
		stats.add(new Stat(48, "Block Value"));
	}
	
	public static StatSystem getInstance() {
		if (instance == null)
			instance = new StatSystem();
		
		return instance;
	}
	
	public ArrayList<String> getStatTypes() {
		ArrayList<String> types = new ArrayList<String>();
		
		for (Stat stat : stats) {
			types.add(stat.getType());
		}
		
		return types;
	}
	
	public ArrayList<Integer> getStatIds() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		for (Stat stat : stats) {
			ids.add(stat.getId());
		}
		
		return ids;
	}
	
	public int getStatId(String type) {
		for (Stat stat : stats) {
			if (stat.getType().equals(type)) {
				return stat.getId();
			}
		}
		return -1;
	}
	
	public String getStatType(int id) {
		for (Stat stat : stats) {
			if (stat.getId() == id) {
				return stat.getType();
			}
		}
		return "";
	}
}
